#!/usr/bin/env bash
set -e
if [ "$#" -ne 3 ]; then
    echo "usage sh create-update-s3-bucket.sh environment <aws cli profile name> <aws default region>"
    exit 1
fi
environment=$1
profileName=$2
region=$3

cfn-create-or-update \
  --stack-name egnikai-candidate-ui-${environment} \
  --template-body file://buildAndDeploy/s3-bucket.yaml \
  --parameters ParameterKey=tagName,ParameterValue=${environment} \
  --region ${region} \
  --profile ${profileName} \
  --wait

aws s3 sync ./public s3://egnikai-web-candidate-ui-${environment} --delete
