module.exports = {
  plugins: {
    postcss: {
      processors: [
        require('autoprefixer')
      ]
    }
  }
};
