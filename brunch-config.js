// See http://brunch.io for documentation.
const postCSSConfig = require('./postcss.config');

module.exports = {
  files: {
    javascripts: {
      joinTo: {
        'app.js': /^app/,
        'vendor.js': /^node_modules/
      }
    },
    stylesheets: {
      joinTo: {
        'app.css': /^app/
      }
    }
  },
  plugins: {
    postcss: postCSSConfig,
    htmlPages: {
      destination(path) {
        return path.replace(/^app[\/\\](.*)\.html$/, '$1.html');
      }
    }
  },
  npm: {
    enabled: true
  }
};
