import React from 'react';
import {shallow} from 'enzyme';
import {Button} from '../../../app/components/Button/Button';

describe('Button', () => {
  it('Button class', () => {
    const button = shallow(
      <Button />
    );
    expect(button.hasClass('button')).toEqual(true);
  });
});

