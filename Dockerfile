FROM nginx
COPY ./public/ /usr/share/nginx/html/
RUN rm -f /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d/
EXPOSE 80
ENV NGINX_PORT 80
CMD ["nginx", "-g", "daemon off;"]