import {fork, all} from 'redux-saga/lib/effects';

import {submitSaga, logoutSaga} from './pages/login/loginSaga';
import {initializeChallengeSaga, initializeChallengePollSaga} from './pages/challenge/challengeSaga';
import {
  loadActiveProblemSaga,
  fetchFileSaga,
  runProblemSaga,
  runProblemPollSaga,
  saveProblemSaga
} from './pages/problems/problemsSaga';

export default function* rootSaga() {
  yield all([
    fork(submitSaga),
    fork(logoutSaga),
    fork(initializeChallengeSaga),
    fork(initializeChallengePollSaga),
    fork(loadActiveProblemSaga),
    fork(fetchFileSaga),
    fork(runProblemSaga),
    fork(runProblemPollSaga),
    fork(saveProblemSaga)
  ]);
}
