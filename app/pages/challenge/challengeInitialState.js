import {setPromiseState} from '../../utils/apiUtils';

const challengeInitialState = {
  challengeInitPromise: setPromiseState(),
  challengeFetchPromise: setPromiseState(),
  attemptedProblemsFetchPromise: setPromiseState(),
  isOverlayVisible: true,
  pollItem: '',
  isLogoutConfirmVisible: false,
  attemptedProblems: [],
  platformName: 'p'
};
export default challengeInitialState;
