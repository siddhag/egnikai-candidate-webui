import {call, put, take, race, takeEvery, select} from 'redux-saga/lib/effects';

import apiSettings from '../../config/apiSettings';
import {
  apiRequest,
  delay,
  isPollSuccessful,
  isPollFailure,
  isPollCompleted
} from '../../utils/apiUtils';
import loginActionTypes from '../login/loginActionTypes';
import {
  lockUserOut
} from '../login/loginAction';
import challengeActionTypes from './challengeActionTypes';
import {showError} from '../../widgets/notification/notificationAction';
import {pollIntervals, ERRORS} from '../../config/constants';

import {
  challengeInitializationPending,
  challengeInitializationFulfilled,
  challengeInitializationRejected,
  challengeFetchPending,
  challengeFetchFulfilled,
  challengeFetchRejected,
  saveOperationId,
  startPoll,
  stopPoll,
  saveAttemptedProblems,
  attemptedProblemsFetchPending,
  attemptedProblemsFetchFulfilled,
  attemptedProblemsFetchRejected,
  setPlatformName
} from './challengeAction';
import {getOperationIDSelector} from './challengeSelector';

import {
  setProblems
} from '../problems/problemsAction';

function* initializeChallenge() {
  try {
    yield put(challengeInitializationPending());
    const {result} = yield call(apiRequest, {
      url: apiSettings.initChallenge,
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'PUT'
      }
    });
    yield put(saveOperationId(result.operationId));
    yield put(startPoll());
  } catch (err) {
    if (err.errors === ERRORS.UNAUTHORIZED) {
      yield put(lockUserOut());
    } else {
      yield put(showError());
    }
    yield put(challengeInitializationRejected());
  }
}

function* getChallenge() {
  try {
    yield put(challengeFetchPending());
    const {result} = yield call(apiRequest, {
      url: apiSettings.getChallenge,
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET'
      }
    });
    const parsedResult = result.problemSummaryDTOs &&
              result.problemSummaryDTOs.reduce((acc, problem) => {
                acc[problem.problemId] = problem;
                return acc;
              }, {});
    yield put(setProblems(parsedResult));
    yield put(setPlatformName(result.platformName));
    yield put(challengeFetchFulfilled());
  } catch (err) {
    if (err.errors === ERRORS.UNAUTHORIZED) {
      yield put(lockUserOut());
    } else {
      yield put(showError());
    }
    yield put(challengeFetchRejected());
  }
}

function* getAttemptedProblems() {
  try {
    yield put(attemptedProblemsFetchPending());
    const {result} = yield call(apiRequest, {
      url: apiSettings.getAttemptedProblems,
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET'
      }
    });

    yield put(saveAttemptedProblems(result));
    yield put(attemptedProblemsFetchFulfilled());
  } catch (err) {
    yield put(attemptedProblemsFetchRejected());
  }
}

function* pollAction(result) {
  const status = result.operationStatus;
  if (!isPollCompleted(status)) {
    return false;
  }
  if (isPollSuccessful(status)) {
    yield put(challengeInitializationFulfilled());
  } else if (isPollFailure(status)) {
    yield put(challengeInitializationRejected());
    yield put(showError());
  }
  yield put(stopPoll());
  return true;
}

function* pollSagaWorker() {
  while (true) {
    yield call(delay, pollIntervals.CHALLENGE_POLL);
    try {
      const operationId = yield select(getOperationIDSelector);
      const {result} = yield call(apiRequest, {
        url: apiSettings.poll([operationId]),
        options: {
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'GET'
        }
      });
      yield call(pollAction, result[0]);
    } catch (err) {
      if (err.errors === ERRORS.UNAUTHORIZED) {
        yield put(lockUserOut());
      }
      console.error(err);
    }
  }
}

function* initializeChallengePollSaga() {
  while (true) {
    yield take(challengeActionTypes.START_CHALLENGE_POLL);
    yield race({
      task: call(pollSagaWorker),
      cancel: take([challengeActionTypes.STOP_CHALLENGE_POLL,
        loginActionTypes.LOGOUT])
    });
  }
}

function* initializeChallengeSaga() {
  yield takeEvery(loginActionTypes.LOGIN_SUCCESS, initializeChallenge);
  yield takeEvery(loginActionTypes.LOGIN_SUCCESS, getChallenge);
  yield takeEvery(loginActionTypes.LOGIN_SUCCESS, getAttemptedProblems);
}

export {initializeChallengeSaga, initializeChallengePollSaga};
