import {connect} from 'react-redux';

import Challenge from './Challenge';
import {hideOverlay, showLogoutConfirmDialog, hideLogoutConfirmDialog} from './challengeAction';
import {logout} from '../../pages/login/loginAction';
import AuthorizationCheck from '../../components/AuthorizationCheck/AuthorizationCheck';

const mapStateToProps = state => ({
  problems: state.problems.items,
  isLocked: state.login.isLocked,
  isChallengeInitPollSuccess: state.challenge.challengeInitPromise.isFulfilled,
  isChallengeFetchSuccess: state.challenge.challengeFetchPromise.isFulfilled,
  isOverlayVisible: state.challenge.isOverlayVisible,
  isLogoutConfirmVisible: state.challenge.isLogoutConfirmVisible,
  attemptedProblems: state.challenge.attemptedProblems
});

const mapDispatchToProps = dispatch => ({
  hideOverlay: () => {
    dispatch(hideOverlay());
  },
  showLogoutConfirmDialog: () => {
    dispatch(showLogoutConfirmDialog());
  },
  hideLogoutConfirmDialog: () => {
    dispatch(hideLogoutConfirmDialog());
  },
  logout: () => {
    dispatch(logout(true));
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationCheck(Challenge));
