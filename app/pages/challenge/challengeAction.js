import challengeActionTypes from './challengeActionTypes';

const challengeInitializationPending = () => ({
  type: challengeActionTypes.CHALLENGE_INIT.pending
});

const challengeInitializationFulfilled = () => ({
  type: challengeActionTypes.CHALLENGE_INIT.fulfilled
});

const challengeInitializationRejected = () => ({
  type: challengeActionTypes.CHALLENGE_INIT.rejected
});

const challengeFetchPending = () => ({
  type: challengeActionTypes.CHALLENGE_FETCH.pending
});

const challengeFetchFulfilled = () => ({
  type: challengeActionTypes.CHALLENGE_FETCH.fulfilled
});

const challengeFetchRejected = () => ({
  type: challengeActionTypes.CHALLENGE_FETCH.rejected
});

const attemptedProblemsFetchPending = () => ({
  type: challengeActionTypes.ATTEMTED_PROBLEMS_FETCH.pending
});

const attemptedProblemsFetchFulfilled = () => ({
  type: challengeActionTypes.ATTEMTED_PROBLEMS_FETCH.fulfilled
});

const attemptedProblemsFetchRejected = () => ({
  type: challengeActionTypes.ATTEMTED_PROBLEMS_FETCH.rejected
});

const hideOverlay = () => ({
  type: challengeActionTypes.HIDE_OVERLAY
});

const saveOperationId = operationId => ({
  type: challengeActionTypes.SAVE_OPERATION_ID,
  data: {operationId}
});

const startPoll = () => ({
  type: challengeActionTypes.START_CHALLENGE_POLL
});

const stopPoll = () => ({
  type: challengeActionTypes.STOP_CHALLENGE_POLL
});

const showLogoutConfirmDialog = () => ({
  type: challengeActionTypes.SHOW_LOGOUT_CONFIRM_DIALOG
});

const hideLogoutConfirmDialog = () => ({
  type: challengeActionTypes.HIDE_LOGOUT_CONFIRM_DIALOG
});

const saveAttemptedProblems = attemptedProblems => ({
  type: challengeActionTypes.SAVE_ATTEMPTED_PROBLEM,
  data: {attemptedProblems}
});

const setPlatformName = platformName => ({
  type: challengeActionTypes.SET_PLATFORM_NAME,
  data: {platformName}
});


export {
  challengeInitializationPending,
  challengeInitializationFulfilled,
  challengeInitializationRejected,
  challengeFetchPending,
  challengeFetchFulfilled,
  challengeFetchRejected,
  hideOverlay,
  saveOperationId,
  startPoll,
  stopPoll,
  showLogoutConfirmDialog,
  hideLogoutConfirmDialog,
  saveAttemptedProblems,
  attemptedProblemsFetchPending,
  attemptedProblemsFetchFulfilled,
  attemptedProblemsFetchRejected,
  setPlatformName
};
