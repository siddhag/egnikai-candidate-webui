const getOperationIDSelector = state => state.challenge.operationId || '';

export {
  getOperationIDSelector
};
