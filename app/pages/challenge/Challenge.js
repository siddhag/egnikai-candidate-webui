import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import Problem from '../../widgets/problem/Problem';
import NavBar from '../../widgets/navbar/NavBarContainer';
import Overlay from './overlay/Overlay';

import {Button} from '../../components/Button/Button';
import Modal from '../../components/Modal/Modal';
import {appRoutes} from '../../config/constants';

function findByassociatedProblemId(items, id) {
  return items.find(item => item.associatedProblemId === id);
}

class Challenge extends Component {
  mayBeRenderProblems() {
    const problems = this.props.problems;
    const attemptedProblems = this.props.attemptedProblems;

    return Object.values(problems).map(problem => (<Problem
      key={problem.problemId}
      passed={findByassociatedProblemId(attemptedProblems, problem.problemId) ?
        findByassociatedProblemId(attemptedProblems, problem.problemId).passed : null}
      {...problem}
    />));
  }

  mayBeRenderChallenge() {
    const {
      showLogoutConfirmDialog,
      isLogoutConfirmVisible
    } = this.props;
    return (
      <div className="hero-body">
        <div className="container">
          <h1 className="title is-size-2 has-text-weight-semibold">
            Problems
          </h1>
          <section className="columns is-multiline">
            {this.mayBeRenderProblems()}
          </section>
          <section className="columns">
            <div className="column is-12 is-horizontal-end">
              <Button
                onClick={showLogoutConfirmDialog}
              >
                Finish
              </Button>
            </div>
          </section>
          {isLogoutConfirmVisible && this.mayBeRenderLogoutConfirm()}
        </div>
      </div>
    );
  }

  mayBeRenderLogoutConfirm() {
    const {
      hideLogoutConfirmDialog,
      logout
    } = this.props;
    return (
      <Modal>
        <div className="text-center">
          Are you sure you want to finish the challenge and logout?
        </div>
        <div className="columns is-centered m-t-25">
          <Link className="link-anchor" to={appRoutes.THANK_YOU}>
            <Button onClick={logout}>
              Yes
            </Button>
          </Link>
          <Button
            className="button m-l-20"
            onClick={hideLogoutConfirmDialog}
          >
            No
          </Button>
        </div>
      </Modal>
    );
  }

  render() {
    const {
      isChallengeInitPollSuccess,
      isChallengeFetchSuccess,
      isOverlayVisible,
      hideOverlay
    } = this.props;
    return ([isOverlayVisible && <Overlay
      key="overlay"
      isChallengeInitPollSuccess={isChallengeInitPollSuccess}
      isChallengeFetchSuccess={isChallengeFetchSuccess}
      hideOverlay={hideOverlay}
    />,
      <section key="problems" className={`hero page is-fullheight ${isOverlayVisible ? 'blur' : ''}`}>
        <NavBar />
        {isChallengeInitPollSuccess && isChallengeFetchSuccess && this.mayBeRenderChallenge()}
      </section>
    ]);
  }
}

Challenge.propTypes = {
  problems: PropTypes.object,
  isChallengeInitPollSuccess: PropTypes.bool,
  isChallengeFetchSuccess: PropTypes.bool,
  isOverlayVisible: PropTypes.bool,
  hideOverlay: PropTypes.func,
  isLogoutConfirmVisible: PropTypes.bool,
  showLogoutConfirmDialog: PropTypes.func,
  hideLogoutConfirmDialog: PropTypes.func,
  logout: PropTypes.func,
  attemptedProblems: PropTypes.array
};

Challenge.defaultProps = {
  problems: {},
  isChallengeInitPollSuccess: false,
  isChallengeFetchSuccess: false,
  isOverlayVisible: true,
  hideOverlay: null,
  isLogoutConfirmVisible: false,
  showLogoutConfirmDialog: null,
  hideLogoutConfirmDialog: null,
  logout: null,
  attemptedProblems: []
};

export default Challenge;
