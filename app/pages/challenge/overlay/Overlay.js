import React from 'react';
import PropTypes from 'prop-types';

import {Button} from '../../../components/Button/Button';
import Loader from '../../../widgets/loader/Loader';

const Overlay = (props) => {
  const isDisabled = () => {
    const {
      isChallengeInitPollSuccess,
      isChallengeFetchSuccess
    } = props;
    return !isChallengeInitPollSuccess || !isChallengeFetchSuccess;
  };
  const {
    hideOverlay
  } = props;
  return (
    <Loader>
      <section className="hero page challenge-overlay is-fullheight">
        <div className="hero-body">
          <div className="container">
            <h1 className="title is-size-2 has-text-weight-bold">
              &quot;The life of a designer is a life of fight against the ugliness&quot;
            </h1>
            <Button
              disabled={isDisabled()}
              onClick={hideOverlay}
              className="login-button"
            >
              Start your <span className="has-text-weight-semibold">&nbsp;60 mins</span>
            </Button>
          </div>
        </div>
      </section>
    </Loader>
  );
};


Overlay.propTypes = {
  isChallengeInitPollSuccess: PropTypes.bool,
  isChallengeFetchSuccess: PropTypes.bool,
  hideOverlay: PropTypes.func
};

Overlay.defaultProps = {
  isChallengeInitPollSuccess: false,
  isChallengeFetchSuccess: false,
  hideOverlay: null
};

export default Overlay;

