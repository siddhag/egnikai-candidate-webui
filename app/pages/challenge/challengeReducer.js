import challengeInitialState from './challengeInitialState';
import challengeActionTypes from './challengeActionTypes';
import {setPromiseState} from '../../utils/apiUtils';

const challengeReducer = (state = challengeInitialState, action) => {
  switch (action.type) {
    case challengeActionTypes.CHALLENGE_INIT.pending:
      return {...state, challengeInitPromise: setPromiseState(true, false, false)};
    case challengeActionTypes.CHALLENGE_INIT.fulfilled:
      return {...state, challengeInitPromise: setPromiseState(false, true, false)};
    case challengeActionTypes.CHALLENGE_INIT.rejected:
      return {...state, challengeInitPromise: setPromiseState(false, false, true)};
    case challengeActionTypes.CHALLENGE_FETCH.pending:
      return {...state, challengeFetchPromise: setPromiseState(true, false, false)};
    case challengeActionTypes.CHALLENGE_FETCH.fulfilled:
      return {...state, challengeFetchPromise: setPromiseState(false, true, false)};
    case challengeActionTypes.CHALLENGE_FETCH.rejected:
      return {...state, challengeFetchPromise: setPromiseState(false, false, true)};
    case challengeActionTypes.HIDE_OVERLAY:
      return {...state, isOverlayVisible: false};
    case challengeActionTypes.SAVE_OPERATION_ID:
      return {...state, operationId: action.data.operationId};
    case challengeActionTypes.SHOW_LOGOUT_CONFIRM_DIALOG:
      return {...state, isLogoutConfirmVisible: true};
    case challengeActionTypes.HIDE_LOGOUT_CONFIRM_DIALOG:
      return {...state, isLogoutConfirmVisible: false};
    case challengeActionTypes.ATTEMTED_PROBLEMS_FETCH.pending:
      return {...state, attemptedProblemsFetchPromise: setPromiseState(true, false, false)};
    case challengeActionTypes.ATTEMTED_PROBLEMS_FETCH.fulfilled:
      return {...state, attemptedProblemsFetchPromise: setPromiseState(false, true, false)};
    case challengeActionTypes.ATTEMTED_PROBLEMS_FETCH.rejected:
      return {...state, attemptedProblemsFetchPromise: setPromiseState(false, false, true)};
    case challengeActionTypes.SAVE_ATTEMPTED_PROBLEM:
      return {...state, attemptedProblems: action.data.attemptedProblems};
    case challengeActionTypes.SET_PLATFORM_NAME:
      return {...state, platformName: action.data.platformName};
    default:
      return state;
  }
};

export default challengeReducer;
