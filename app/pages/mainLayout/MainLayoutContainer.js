import {connect} from 'react-redux';

import MainLayout from './MainLayout';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);
