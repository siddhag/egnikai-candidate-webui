import React from 'react';
import PropTypes from 'prop-types';
import NotificationContainer from '../../widgets/notification/NotificationContainer';

const MainLayout = (props) => {
  const {children} = props;
  return (
    <section>
      <NotificationContainer />
      {children}
    </section>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node
};

MainLayout.defaultProps = {
  children: null
};

export default MainLayout;
