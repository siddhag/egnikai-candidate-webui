import React from 'react';
import PropTypes from 'prop-types';
import {Link, Redirect} from 'react-router-dom';

import {Button} from '../../components/Button/Button';
import NavBar from '../../widgets/navbar/NavBarContainer';
import Loader from '../../widgets/loader/Loader';

import { appRoutes } from '../../config/constants';

function mayBeRenderLoader(isAttemptedProblemsFetchPromisePending) {
  if (isAttemptedProblemsFetchPromisePending) {
    return (<Loader className="is-loading" />);
  }
  return null;
}

function mayBeRedirectToChallenge(isAttemptedProblemsFetchPromisePending, attemptedProblems) {
  const hasAttemptedProblems = attemptedProblems.length > 0;
  if (!isAttemptedProblemsFetchPromisePending && hasAttemptedProblems) {
    return (<Redirect to={appRoutes.CHALLENGE} />);
  }
  return null;
}

const Welcome = (props) => {
  const {username, isAttemptedProblemsFetchPromisePending, attemptedProblems} = props;
  return (
    <section className="hero page welcome is-fullheight">
      <NavBar />
      <div className="hero-body">
        <div className="container">
          <h1 className="title is-size-2 has-text-weight-semibold">
            Hello {username}
          </h1>
          <h1 className="title is-size-2 has-text-weight-light">
            Welcome to Egnikai.
          </h1>
          <Link className="link-anchor" to="/instructions">
            <Button>
              <span className="is-size-6">Next:</span>
              <span className="is-size-6 p-l-5">Read instructions</span>
            </Button>
          </Link>
        </div>
      </div>
      {mayBeRenderLoader(isAttemptedProblemsFetchPromisePending)}
      {mayBeRedirectToChallenge(isAttemptedProblemsFetchPromisePending, attemptedProblems)}
    </section>
  );
};

Welcome.propTypes = {
  username: PropTypes.string,
  isAttemptedProblemsFetchPromisePending: PropTypes.bool,
  attemptedProblems: PropTypes.array
};

Welcome.defaultProps = {
  username: ''
};

export default Welcome;
