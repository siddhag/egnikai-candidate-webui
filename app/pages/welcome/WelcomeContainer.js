import {connect} from 'react-redux';

import Welcome from './Welcome';
import AuthorizationCheck from '../../components/AuthorizationCheck/AuthorizationCheck';

const mapStateToProps = state => ({
  username: state.login.credentials.name,
  isLocked: state.login.isLocked,
  isAttemptedProblemsFetchPromisePending: state.challenge.attemptedProblemsFetchPromise.isPending,
  attemptedProblems: state.challenge.attemptedProblems,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationCheck(Welcome));
