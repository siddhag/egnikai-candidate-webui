const loginActionTypes = {
  SHOW_ERROR: 'login/SHOW_ERROR',
  LOGIN_REQUEST: 'login/LOGIN_REQUEST',
  SET_CREDENTIALS: 'login/SET_CREDENTIALS',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOCK_USER_OUT: 'login/LOCK_USER_OUT',
  LOGIN: {
    pending: 'LOGIN/pending',
    fulfilled: 'LOGIN/fulfilled',
    rejected: 'LOGIN/rejected'
  },
  LOGOUT: 'LOGOUT'
};

export default loginActionTypes;
