import {setPromiseState} from '../../utils/apiUtils';

const loginInitialState = {
  isLoggedIn: false,
  showError: false,
  isLocked: false,
  credentials: {
    id: '',
    name: ''
  },
  promise: setPromiseState()
};
export default loginInitialState;
