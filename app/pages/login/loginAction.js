import loginActionTypes from './loginActionTypes';

const onLoginSuccess = () => ({
  type: loginActionTypes.LOGIN_SUCCESS
});

const showError = status => ({
  type: loginActionTypes.SHOW_ERROR,
  data: {visibility: status}
});

const setCredentials = (id, name) => ({
  type: loginActionTypes.SET_CREDENTIALS,
  data: {id, name}
});

const loginPending = () => ({
  type: loginActionTypes.LOGIN.pending
});

const loginFulfilled = () => ({
  type: loginActionTypes.LOGIN.fulfilled
});

const lockUserOut = () => ({
  type: loginActionTypes.LOCK_USER_OUT
});

const loginRejected = () => ({
  type: loginActionTypes.LOGIN.rejected
});

const logout = (shouldCandidateBeLocked = false) => ({
  type: loginActionTypes.LOGOUT,
  data: {
    shouldCandidateBeLocked
  }
});

const loginRequest = data => ({type: loginActionTypes.LOGIN_REQUEST, data});

export {
  lockUserOut,
  loginRequest,
  onLoginSuccess,
  showError,
  setCredentials,
  loginPending,
  loginFulfilled,
  loginRejected,
  logout
};
