import loginInitialState from './loginInitialState';
import loginActionTypes from './loginActionTypes';
import {setPromiseState} from '../../utils/apiUtils';

const loginReducer = (state = loginInitialState, action) => {
  switch (action.type) {
    case loginActionTypes.LOGIN_SUCCESS:
      return {...state, isLoggedIn: true};
    case loginActionTypes.LOGOUT:
      return {...state, isLoggedIn: false};
    case loginActionTypes.LOCK_USER_OUT:
      return {...state, isLocked: true, isLoggedIn: false};
    case loginActionTypes.SHOW_ERROR:
      return {...state, showError: action.data.visibility};
    case loginActionTypes.SET_CREDENTIALS: {
      const {id, name} = action.data;
      return {...state, credentials: {id, name}};
    }
    case loginActionTypes.LOGIN.pending:
      return {...state, promise: setPromiseState(true, false, false)};
    case loginActionTypes.LOGIN.fulfilled:
      return {...state, promise: setPromiseState(false, true, false)};
    case loginActionTypes.LOGIN.rejected:
      return {...state, promise: setPromiseState(false, false, true)};
    default:
      return state;
  }
};

export default loginReducer;
