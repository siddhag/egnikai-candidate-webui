import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';

import {Button} from '../../components/Button/Button';
import TextInput from '../../components/TextInput/TextInput';
import { ERROR_MESSAGES, appRoutes } from '../../config/constants';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
    this.setUsername = this.setUsername.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.isDisabled = this.isDisabled.bind(this);
  }

  componentDidMount() {
    const {isLoggedIn, isLocked} = this.props;
    if (!isLoggedIn) {
      this.props.logout();
    }
    if (isLocked) {
      this.props.showErrorNotification(ERROR_MESSAGES.LOCKED_OUT);
    }
  }

  componentWillUnmount() {
    const {isLoggedIn} = this.props;
    if (isLoggedIn) {
      this.props.clearErrorNotification();
    }
  }

  setUsername(username) {
    this.setState({
      username
    });
  }

  setPassword(password) {
    this.setState({
      password
    });
  }

  isDisabled() {
    const {
      username,
      password
    } = this.state;
    const {
      isPromisePending
    } = this.props;
    return (username.length === 0 || password.length === 0 || isPromisePending);
  }

  mayBeRenderError() {
    const {showError} = this.props;
    if (showError) {
      return (
        <div className="has-text-danger">
          *Invalid username or password
        </div>
      );
    }
    return null;
  }

  render() {
    const {loginRequest, isLoggedIn} = this.props;
    const {from} = {from: {pathname: appRoutes.WELCOME}};
    if (isLoggedIn) {
      return (
        <Redirect to={from} />
      );
    }
    return (
      <section className="login page hero is-fullheight">
        <div className="hero-head header">
          <div className="container">
            <h1 className="title is-size-2 has-text-centered has-text-weight-bold">
              Egnikai.
            </h1>
          </div>
        </div>
        <form
          className="hero-body columns is-centered"
          onSubmit={(event) => {
            event.preventDefault();
            const {
              username,
              password
            } = this.state;
            loginRequest({username, password});
          }}
        >
          <div className="column is-4">
            <article className="card">
              <div className="card-content">
                <h1 className="subtitle is-size-2 has-text-centered">
                  Hello.
                </h1>
                {this.mayBeRenderError()}
                <div className="control">
                  <TextInput
                    onChange={this.setUsername}
                    id="username"
                    labelText="User name"
                    type="text"
                  />
                </div>
                <div className="control">
                  <TextInput
                    onChange={this.setPassword}
                    id="password"
                    labelText="Password"
                    type="password"
                  />
                </div>
                <div className="control">
                  <Button
                    disabled={this.isDisabled()}
                    className="login-button"
                  >
                    Login
                  </Button>
                </div>
              </div>
            </article>
          </div>
        </form>
      </section>
    );
  }
}

LoginForm.propTypes = {
  loginRequest: PropTypes.func,
  showError: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  isLocked: PropTypes.bool,
  isPromisePending: PropTypes.bool,
  logout: PropTypes.func,
  showErrorNotification: PropTypes.func.isRequired,
  clearErrorNotification: PropTypes.func.isRequired
};

LoginForm.defaultProps = {
  loginRequest: null,
  isLocked: false,
  showError: false,
  isLoggedIn: false,
  isPromisePending: false,
  logout: null
};

export default LoginForm;
