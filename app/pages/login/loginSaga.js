import {call, put, takeEvery} from 'redux-saga/lib/effects';

import apiSettings from '../../config/apiSettings';
import {apiRequest} from '../../utils/apiUtils';
import loginActionTypes from './loginActionTypes';
import {
  showError,
  setCredentials,
  loginPending,
  loginFulfilled,
  loginRejected,
  onLoginSuccess
} from './loginAction';

function* onSubmit(action) {
  const {username, password} = action.data;
  const delimitedCredentials = `${username}:${password}`;

  try {
    yield put(loginPending());
    const {result} = yield call(apiRequest, {
      url: apiSettings.login,
      options: {
        credentials: 'include',
        mode: 'cors',
        headers: {
          Authorization: `Basic ${btoa(delimitedCredentials)}`,
          'Content-Type': 'application/json'
        },
        method: 'POST'
      }
    });
    yield put(onLoginSuccess());
    yield put(showError(false));
    yield put(setCredentials(result.userid, result.username));
    yield put(loginFulfilled());
  } catch (errors) {
    yield put(showError(true));
    yield put(loginRejected());
  }
}

function* onLogout(action) {
  try {
    yield call(apiRequest, {
      url: apiSettings.logout,
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
          shouldCandidateBeLocked: (action.data || {}).shouldCandidateBeLocked || false,
        }),
      }
    });
  } catch (errors) {
    console.error(errors);
  }
}

function* submitSaga() {
  yield takeEvery(loginActionTypes.LOGIN_REQUEST, onSubmit);
}

function* logoutSaga() {
  yield takeEvery(loginActionTypes.LOGOUT, onLogout);
}

export {submitSaga, logoutSaga};
