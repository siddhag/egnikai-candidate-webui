import {connect} from 'react-redux';

import {loginRequest, logout } from './loginAction';

import LoginForm from './Login';
import {removeAll, showError} from '../../widgets/notification/notificationAction';

const mapStateToProps = state => ({
  showError: state.login.showError,
  isLoggedIn: state.login.isLoggedIn,
  isLocked: state.login.isLocked,
  isPromisePending: state.login.promise.isPending
});

const mapDispatchToProps = dispatch => ({
  loginRequest: (data) => {
    dispatch(loginRequest(data));
  },
  logout: () => {
    dispatch(logout());
  },
  showErrorNotification: (msg) => {
    dispatch(showError(msg));
  },
  clearErrorNotification: () => {
    dispatch(removeAll());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
