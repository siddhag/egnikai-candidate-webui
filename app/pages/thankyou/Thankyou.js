import React from 'react';

const Thankyou = () => (
  <section className="hero is-fullheight thankyou">
    <div className="hero-body">
      <div className="container">
        <div className="thankyou-container center-block">
          <h1 className="is-size-1">
            Looks like you are done.
          </h1>
          <h1 className="is-size-1 has-text-weight-bold m-b-10">
            Good Luck!!
          </h1>
          <a className="button is-primary is-large-button" href="https://thoughtworks.wistia.com/medias/m4htfpl01i">Life at ThoughtWorks</a>
        </div>
      </div>
    </div>
  </section>
);


export default Thankyou;
