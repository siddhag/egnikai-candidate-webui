import React from 'react';
import {Link} from 'react-router-dom';

import {Button} from '../../components/Button/Button';
import NavBar from '../../widgets/navbar/NavBarContainer';

const instruction = () => (
  <section className="hero page instruction is-fullheight">
    <NavBar />
    <div className="hero-body">
      <div className="container">
        <h1 className="title is-size-2 has-text-weight-semibold">
          Instructions
        </h1>
        <aside className="menu">
          <ul className="menu-list">
            <li>1. Duration 60 Minutes.</li>
            <li>2. Attempt as many problems as you can, but a minimum
              of 5 needs to be solved.</li>
            <li>3. Out of the given problems,
              at least 2 of Problems marked mandatory need to be solved</li>
            <li>4. Do not make any modification in the test folders.</li>
            <li>5. A problem will be considered solved only when all
              the tests for that are green.
            </li>
            <li>6. Scores will be evaluated based on number of problems solved.</li>
          </ul>
        </aside>
        <Link className="link-anchor" to="/challenge">
          <Button>
            <span className="is-size-6">Next:</span>
          </Button>
        </Link>
      </div>
    </div>
  </section>
);

export default instruction;
