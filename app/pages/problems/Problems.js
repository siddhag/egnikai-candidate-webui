import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Mousetrap from 'mousetrap';
import {Link} from 'react-router-dom';
import {Button} from '../../components/Button/Button';
import CodeEditor from '../../components/CodeEditor/CodeEditor';
import NavBar from '../../widgets/navbar/NavBarContainer';
import Loader from '../../widgets/loader/Loader';
import {fileTypeNamespace, operationStatus, operationName, appRoutes} from '../../config/constants';

class Problems extends Component {

  static isResultSuccessful(result) {
    return result === operationStatus.ACCEPTED;
  }

  static isResultFailure(result) {
    return result === operationStatus.REJECTED;
  }

  static isSourceFile(fileType) {
    return fileType === fileTypeNamespace.SOURCE;
  }

  static mayBeRenderSaveResult() {
    const message = 'Save failed';
    const customClass = 'is-danger';
    return (
      <div
        className={`tag is-small m-l-15 ${customClass}`}
      >
        {message}
      </div>
    );
  }

  constructor(props) {
    super(props);
    this.onUserInput = this.onUserInput.bind(this);
    this.saveUndoManagerState = this.saveUndoManagerState.bind(this);
    this.saveFile = this.saveFile.bind(this);
  }


  componentDidMount() {
    Mousetrap.bind(['mod+s'], this.saveFile);
    const {problem, sourceFiles, testFiles} = this.props;
    if (!sourceFiles.length || !testFiles.length) {
      this.props.loadActiveProblem(problem.problemId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {activeFile, fetchFile, problem} = this.props;
    const {activeFile: newActiveFile} = nextProps;
    if (activeFile.fileId !== newActiveFile.fileId ||
      activeFile.fileType !== newActiveFile.fileType) {
      if (newActiveFile.fileContent === '') {
        fetchFile({
          problemId: problem.problemId,
          isSourceFile: Problems.isSourceFile(newActiveFile.fileType),
          fileId: newActiveFile.fileId
        });
      }
    }
  }

  componentWillUnmount() {
    Mousetrap.unbind(['ctrl+s'], this.saveFile);
  }

  onUserInput(newValue) {
    const {problem, activeFile, setContentToFile, markFileAsChanged} = this.props;
    const problemId = problem.problemId;
    const {fileId, fileType} = activeFile;
    const isSourceFile = Problems.isSourceFile(fileType);
    setContentToFile({
      problemId,
      isSourceFile,
      fileId,
      result: newValue
    });
    markFileAsChanged({problemId, isSourceFile, fileId, isDirty: true});
  }

  saveFile() {
    const {isProblemSavePending, isUnsaved, problem, saveProblem} = this.props;
    if (isProblemSavePending || !isUnsaved) return false;
    saveProblem(problem.problemId);
    return false;
  }

  mayBeRenderLoader() {
    const {
      isFileNamesFetchPromisePending,
      isFetchFilePromisePending
    } = this.props;
    if (isFileNamesFetchPromisePending || isFetchFilePromisePending) {
      return (<Loader className="is-loading" />);
    }
    return null;
  }

  isActiveFile(id, fileType) {
    const {activeFile} = this.props;
    if (!activeFile) {
      return false;
    }
    return id.toString() === activeFile.fileId.toString() && fileType === activeFile.fileType;
  }

  mayBeRenderSourceFileTabs() {
    const {sourceFiles, problem} = this.props;
    if (!sourceFiles.length) {
      return null;
    }
    const items = this.renderTabs({
      problemId: problem.problemId,
      fileType: fileTypeNamespace.SOURCE,
      files: sourceFiles
    });
    return (<aside className="files-list-source menu">
      <div className="is-size-5">Source Files</div>
      <ul className="menu-list">{items}</ul>
    </aside>);
  }

  mayBeRenderTestFileTabs() {
    const {testFiles, problem} = this.props;
    if (!testFiles.length) {
      return null;
    }
    const items = this.renderTabs({
      problemId: problem.problemId,
      fileType: fileTypeNamespace.TEST,
      files: testFiles
    });
    return (<aside className="files-list-test menu">
      <div className="is-size-5">Test Cases</div>
      <ul className="menu-list">{items}</ul>
    </aside>);
  }

  mayBeRenderTestResult() {
    const {testResult} = this.props.problem;
    let message = null;
    let customClass = null;
    if (!testResult) {
      return null;
    }
    const result = testResult.operationStatus;
    if (Problems.isResultSuccessful(result)) {
      message = 'Test case passed';
      customClass = 'is-success';
    } else if (Problems.isResultFailure(result)) {
      if (testResult.operationName === operationName.COMPILE) {
        message = 'Compilation error';
      } else {
        message = 'Test case failed';
      }
      customClass = 'is-danger';
    }
    if (message && customClass) {
      return (
        <div
          className={`tag is-small m-l-15 ${customClass}`}
        >
          {message}
        </div>
      );
    }
    return null;
  }

  saveUndoManagerState(args) {
    const {problemId, fileId, fileType, undoManagerState} = args;
    const isSourceFile = Problems.isSourceFile(fileType);
    this.props.saveUndoManagerState({problemId, isSourceFile, fileId, undoManagerState});
  }

  renderTabs({problemId, fileType, files}) {
    const {setActiveFile} = this.props;
    return files.map((file) => {
      const {id, name, isDirty, isEditable} = file;
      return (<li
        className={`is-text-overflow
        ${this.isActiveFile(id, fileType) ? 'active-file' : ''}
        ${isDirty ? 'unsaved-file has-text-weight-bold' : ''}
        ${!isEditable ? 'has-text-grey-light' : ''}`
        }
        key={id}
        title={name}
        onClick={() => {
          const isSourceFile = fileType === fileTypeNamespace.SOURCE;
          setActiveFile({problemId, isSourceFile, fileId: id});
        }}
      >
        {name}
      </li>);
    });
  }

  render() {
    const {
      problem,
      activeFile,
      runProblem,
      isProblemRunPending,
      isUnsaved,
      saveProblem,
      isProblemSavePending,
      items,
      platformName
    } = this.props;
    const problemId = problem.problemId;

    return (<section className="hero page problems is-fullheight">
      <NavBar />
      <div className="hero-body">
        <div className="container">
          <section>
            <h1
              className={`title column is-size-2 has-text-weight-semibold
              is-vertical-center is-marginless is-paddingless`}
            >
              Problem
              {this.mayBeRenderTestResult()}
            </h1>
          </section>
          <p className="subtitle m-b-25">{problem.description}</p>
          <div className="m-b-20">
            <Link className="is-primary-link" to={appRoutes.CHALLENGE}>
              &lt;&nbsp; See all problems
            </Link>
          </div>
          <div className="problem-view columns is-marginless is-paddingless">
            <div className="files-list column is-2">
              {this.mayBeRenderSourceFileTabs()}
              {this.mayBeRenderTestFileTabs()}
            </div>
            <div className="column is-marginless is-paddingless">
              <CodeEditor
                value={activeFile.fileContent}
                activeFile={activeFile}
                problemId={problem.problemId}
                readOnly={!activeFile.isFileEditable}
                onChange={this.onUserInput}
                saveUndoManagerState={this.saveUndoManagerState}
                undoManagerState={activeFile.undoManagerState}
                saveFile={this.saveFile}
                mode={platformName.toLowerCase()}
              />
            </div>
          </div>
          <div className="m-t-20 m-b-20 is-horizontal-end">
            <Button
              className={`m-r-20 ${isProblemSavePending ? 'is-loading' : ''}`}
              disabled={isProblemSavePending || !isUnsaved}
              onClick={() => {
                saveProblem(problemId);
              }}
            >
              Save
            </Button>
            <Button
              className={`${isProblemRunPending ? 'is-loading' : ''}`}
              disabled={isProblemRunPending || isUnsaved || items[problemId].isMaliciousCode}
              onClick={() => {
                runProblem(problemId);
              }}
            >
              Run
            </Button>
          </div>

          {items && items[problemId].isMaliciousCode &&
          <div>
            <h3 className="m-b-15">Result : {Problems.mayBeRenderSaveResult()}
            </h3>
            <div className="card">
              <div className="card-content">
                {items[problemId].message}
              </div>
            </div>
          </div>
          }

          {problem.testResult &&
          <div>
            <h3 className="m-b-15">Result : {this.mayBeRenderTestResult()}
            </h3>
            {problem.testResult.errorInformation &&
            <div className="card">
              <div className="card-content error-result">
                {problem.testResult.errorInformation}
              </div>
            </div>
            }
          </div>
          }
        </div>
      </div>
      {this.mayBeRenderLoader()}
    </section>);
  }
}


export default Problems;

Problems.propTypes = {
  problem: PropTypes.object,
  loadActiveProblem: PropTypes.func,
  activeFile: PropTypes.object,
  isFileNamesFetchPromisePending: PropTypes.bool,
  isFetchFilePromisePending: PropTypes.bool,
  sourceFiles: PropTypes.array,
  saveUndoManagerState: PropTypes.func.isRequired,
  testFiles: PropTypes.array,
  setActiveFile: PropTypes.func,
  fetchFile: PropTypes.func,
  runProblem: PropTypes.func,
  isProblemRunPending: PropTypes.bool,
  setContentToFile: PropTypes.func,
  markFileAsChanged: PropTypes.func,
  isUnsaved: PropTypes.bool,
  saveProblem: PropTypes.func,
  isProblemSavePending: PropTypes.bool,
  items: PropTypes.object,
  platformName: PropTypes.object
};

Problems.defaultProps = {
  problem: {},
  loadActiveProblem: null,
  activeFile: {},
  isFileNamesFetchPromisePending: false,
  isFetchFilePromisePending: false,
  sourceFiles: [],
  testFiles: [],
  setActiveFile: null,
  fetchFile: null,
  runProblem: null,
  isProblemRunPending: false,
  setContentToFile: null,
  markFileAsChanged: null,
  isUnsaved: false,
  saveProblem: null,
  isProblemSavePending: false,
  saveUndoManagerState: null,
  items: {},
  platformName: null
};
