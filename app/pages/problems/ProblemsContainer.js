import {connect} from 'react-redux';

import Problems from './Problems';
import AuthorizationCheck from '../../components/AuthorizationCheck/AuthorizationCheck';
import {
  loadActiveProblem,
  setActiveFile,
  fetchFile,
  runProblem,
  saveProblem,
  setContentToFile,
  markFileAsChanged,
  setCodeStatusAtMalicious,
  saveUndoManagerState
} from './problemsAction';
import {
  getActiveProblem,
  getActiveFileSelector,
  getSourceFilesByProblemSelector,
  getTestFilesByProblemSelector,
  isProblemRunPendingSelector,
  areSourceFilesUnSavedSelector,
  isProblemSavePendingSelector
} from './problemsSelector';

const mapStateToProps = (state, props) => {
  const selectedId = props.match.params.id;
  return {
    problem: getActiveProblem(state, selectedId),
    platformName: state.challenge.platformName,
    isLocked: state.login.isLocked,
    activeFile: getActiveFileSelector(state, selectedId),
    sourceFiles: getSourceFilesByProblemSelector(state, selectedId),
    testFiles: getTestFilesByProblemSelector(state, selectedId),
    isFileNamesFetchPromisePending: state.problems.fileNamesFetchPromise.isPending,
    isFetchFilePromisePending: state.problems.fetchFilePromise.isPending,
    isProblemRunPending: isProblemRunPendingSelector(state, selectedId),
    isUnsaved: areSourceFilesUnSavedSelector(state, selectedId),
    isProblemSavePending: isProblemSavePendingSelector(state, selectedId),
    items: state.problems.items
  };
};

const mapDispatchToProps = dispatch => ({
  loadActiveProblem: (problemId) => {
    dispatch(loadActiveProblem(problemId));
  },
  setActiveFile: (params) => {
    dispatch(setActiveFile(params));
  },
  fetchFile: (params) => {
    dispatch(fetchFile(params));
  },
  runProblem: (params) => {
    dispatch(runProblem(params));
  },
  saveProblem: (problemId) => {
    dispatch(saveProblem(problemId));
  },
  setContentToFile: (params) => {
    dispatch(setContentToFile(params));
  },
  saveUndoManagerState: (params) => {
    dispatch(saveUndoManagerState(params));
  },
  markFileAsChanged: (params) => {
    dispatch(markFileAsChanged(params));
  },
  setCodeStatusAtMalicious: (problemId) => {
    dispatch(setCodeStatusAtMalicious(problemId));
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationCheck(Problems));
