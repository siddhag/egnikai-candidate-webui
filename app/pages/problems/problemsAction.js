import problemsActionTypes from './problemsActionTypes';
import {fileTypeNamespace} from '../../config/constants';

const loadActiveProblem = problemId => ({
  type: problemsActionTypes.LOAD_ACTIVE_PROBLEM,
  data: {problemId}
});

const fetchFileNamesPending = () => ({
  type: problemsActionTypes.FILENAMES_FETCH.pending
});

const fetchFileNamesFulfilled = () => ({
  type: problemsActionTypes.FILENAMES_FETCH.fulfilled
});

const fetchFileNamesRejected = () => ({
  type: problemsActionTypes.FILENAMES_FETCH.rejected
});

const fetchFilePromisePending = () => ({
  type: problemsActionTypes.FETCH_FILE_PROMISE.pending
});

const fetchFilePromiseFulfilled = () => ({
  type: problemsActionTypes.FETCH_FILE_PROMISE.fulfilled
});

const fetchFilePromiseRejected = () => ({
  type: problemsActionTypes.FETCH_FILE_PROMISE.rejected
});

const fetchFile = ({problemId, isSourceFile, fileId}) => ({
  type: problemsActionTypes.FETCH_FILE,
  data: {problemId, isSourceFile, fileId}
});

const setProblems = problems => ({
  type: problemsActionTypes.SET_PROBLEMS,
  data: {problems}
});

const setFilesToProblem = ({problemId, sourceFiles, testFiles}) => ({
  type: problemsActionTypes.SET_FILES_TO_PROBLEM,
  data: {problemId, src: sourceFiles, test: testFiles}
});

const setActiveFile = ({problemId, isSourceFile, fileId}) => {
  const fileType = isSourceFile ? fileTypeNamespace.SOURCE : fileTypeNamespace.TEST;
  return {
    type: problemsActionTypes.SET_ACTIVE_FILE,
    data: {problemId, fileType, fileId}
  };
};

const getFileType = isSourceFile => (isSourceFile ?
  fileTypeNamespace.SOURCE : fileTypeNamespace.TEST);

const setContentToFile = ({problemId, isSourceFile, fileId, result}) => {
  const fileType = getFileType(isSourceFile);
  return {
    type: problemsActionTypes.SET_CONTENT_TO_FILE,
    data: {problemId, fileType, fileId, result}
  };
};

const saveUndoManagerState = ({problemId, isSourceFile, fileId, undoManagerState}) => {
  const fileType = getFileType(isSourceFile);
  return {
    type: 'PROBLEM_SAVE_UNDO_MANAGER',
    data: {problemId, fileType, fileId, undoManagerState}
  };
};

const runProblem = problemId => ({
  type: problemsActionTypes.RUN_PROBLEM,
  data: {problemId}
});

const addItemsToPoll = ({problemId, operationId}) => ({
  type: problemsActionTypes.ADD_ITEM_TO_POLL,
  data: {problemId, operationId}
});

const removeItemFromPoll = ({problemId, operationId}) => ({
  type: problemsActionTypes.REMOVE_ITEM_FROM_POLL,
  data: {problemId, operationId}
});

const setTestResult = ({problemId, testResult}) => ({
  type: problemsActionTypes.SET_TEST_RESULT,
  data: {problemId, testResult}
});

const startRunPoll = () => ({
  type: problemsActionTypes.START_RUN_POLL
});

const stopRunPoll = () => ({
  type: problemsActionTypes.STOP_RUN_POLL
});

const runProblemPromisePending = problemId => ({
  type: problemsActionTypes.RUN_PROBLEM_PROMISE.pending,
  data: problemId
});

const runProblemPromiseFulfilled = problemId => ({
  type: problemsActionTypes.RUN_PROBLEM_PROMISE.fulfilled,
  data: problemId
});

const runProblemPromiseRejected = problemId => ({
  type: problemsActionTypes.RUN_PROBLEM_PROMISE.rejected,
  data: problemId
});

const saveProblem = problemId => ({
  type: problemsActionTypes.SAVE_PROBLEM,
  data: {problemId}
});

const saveProblemPromisePending = problemId => ({
  type: problemsActionTypes.SAVE_PROBLEM_PROMISE.pending,
  data: problemId
});

const saveProblemPromiseFulfilled = (problemId, isMaliciousCode, message) => ({
  type: problemsActionTypes.SAVE_PROBLEM_PROMISE.fulfilled,
  data: {problemId, isMaliciousCode, message}
});

const saveProblemPromiseRejected = problemId => ({
  type: problemsActionTypes.SAVE_PROBLEM_PROMISE.rejected,
  data: problemId
});

const markFileAsChanged = ({problemId, isSourceFile, fileId, isDirty}) => {
  const fileType = getFileType(isSourceFile);
  return {
    type: problemsActionTypes.MARK_FILE_AS_CHANGED,
    data: {problemId, fileType, fileId, isDirty}
  };
};

export {
  loadActiveProblem,
  fetchFileNamesPending,
  fetchFileNamesFulfilled,
  fetchFileNamesRejected,
  setProblems,
  setFilesToProblem,
  fetchFile,
  fetchFilePromisePending,
  fetchFilePromiseFulfilled,
  fetchFilePromiseRejected,
  setContentToFile,
  setActiveFile,
  runProblem,
  addItemsToPoll,
  removeItemFromPoll,
  setTestResult,
  runProblemPromisePending,
  runProblemPromiseFulfilled,
  runProblemPromiseRejected,
  startRunPoll,
  stopRunPoll,
  saveProblem,
  saveProblemPromisePending,
  saveProblemPromiseFulfilled,
  saveProblemPromiseRejected,
  markFileAsChanged,
  saveUndoManagerState
};

