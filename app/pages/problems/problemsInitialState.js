import {setPromiseState} from '../../utils/apiUtils';

const problemsInitialState = {
  items: {},
  fileNamesFetchPromise: setPromiseState(),
  fetchFilePromise: setPromiseState()
};
export default problemsInitialState;
