import {createSelector} from 'reselect';
import {fileTypeNamespace} from '../../config/constants';
import {setPromiseState} from '../../utils/apiUtils';

const getActiveProblem = (state, id) => state.problems.items[id] || {};

const getAllProblems = state => Object.values(state.problems.items) || [];

const getActiveFile = (problem) => {
  const activeFile = problem.activeFile;
  if (!activeFile) {
    return {
      fileId: '',
      fileType: '',
      fileContent: '',
      isFileEditable: true
    };
  }
  const {fileType, fileId} = activeFile;
  const file = problem[fileType][fileId];
  const fileContent = file.content || '';
  return {
    ...problem.activeFile,
    ...{fileContent},
    isFileEditable: file.editable,
    undoManagerState: file.undoManagerState
  };
};

const getSourceFilesByProblem = (problem) => {
  const sourceFileNamespace = fileTypeNamespace.SOURCE;
  if (!problem[sourceFileNamespace]) {
    return [];
  }
  return Object.values(problem[sourceFileNamespace]).map(file => (
    {
      id: file.sourceId,
      name: file.sourceName,
      isDirty: file.isDirty,
      content: file.content,
      isEditable: file.editable
    }
  )
  );
};

const getTestFilesByProblem = (problem) => {
  const testFileNamespace = fileTypeNamespace.TEST;
  if (!problem[testFileNamespace]) {
    return [];
  }
  return Object.values(problem[testFileNamespace]).map(file => (
      {id: file.testId, name: file.testFileName}
    )
  );
};

const getItemsToPoll = problems => problems.reduce((acc, problem) => {
  const pollingItems = problem.pollingItems;
  return Object.assign({}, acc, pollingItems || {});
}, {});

const isProblemRunPending = (problem) => {
  const promise = problem.runProblemPromise || setPromiseState();
  return promise.isPending;
};

const isProblemSavePending = (problem) => {
  const promise = problem.saveProblemPromise || setPromiseState();
  return promise.isPending;
};

const getUnsavedFiles = sourceFiles => sourceFiles.filter(file => file.isDirty === true);

const areSourceFilesUnSaved = unsavedFiles => unsavedFiles.length > 0;

const getProblemIdByOperationIdSelector = (state, operationId) => {
  const problems = getAllProblems(state);
  return problems.filter((problem) => {
    const pollingItems = problem.pollingItems || {};
    return Object.keys(pollingItems).includes(operationId);
  }).reduce((acc, problem) => {
    if (problem) {
      return problem.problemId;
    }
    return acc;
  }, null);
};

const getActiveFileSelector = createSelector(
  [getActiveProblem],
  getActiveFile
);

const getSourceFilesByProblemSelector = createSelector(
  [getActiveProblem],
  getSourceFilesByProblem
);

const getTestFilesByProblemSelector = createSelector(
  [getActiveProblem],
  getTestFilesByProblem
);

const getItemsToPollSelector = createSelector(
  [getAllProblems],
  getItemsToPoll
);

const isProblemRunPendingSelector = createSelector(
  [getActiveProblem],
  isProblemRunPending
);

const isProblemSavePendingSelector = createSelector(
  [getActiveProblem],
  isProblemSavePending
);

const getUnsavedFilesSelector = createSelector(
  [getSourceFilesByProblemSelector],
  getUnsavedFiles
);

const areSourceFilesUnSavedSelector = createSelector(
  [getUnsavedFilesSelector],
  areSourceFilesUnSaved
);

export {
  getActiveProblem,
  getActiveFileSelector,
  getSourceFilesByProblemSelector,
  getTestFilesByProblemSelector,
  getItemsToPollSelector,
  getProblemIdByOperationIdSelector,
  isProblemRunPendingSelector,
  getUnsavedFilesSelector,
  areSourceFilesUnSavedSelector,
  isProblemSavePendingSelector
};
