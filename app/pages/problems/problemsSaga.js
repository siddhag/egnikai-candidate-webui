import {call, put, takeEvery, select, all, takeLatest, race, take} from 'redux-saga/lib/effects';
import problemsActionTypes from './problemsActionTypes';
import {getItemsToPollSelector, getProblemIdByOperationIdSelector, getUnsavedFilesSelector} from './problemsSelector';
import { ERRORS, pollIntervals } from '../../config/constants';

import {
  fetchFileNamesPending,
  fetchFileNamesFulfilled,
  fetchFileNamesRejected,
  setFilesToProblem,
  fetchFile,
  fetchFilePromisePending,
  fetchFilePromiseFulfilled,
  fetchFilePromiseRejected,
  setContentToFile,
  setActiveFile,
  addItemsToPoll,
  removeItemFromPoll,
  setTestResult,
  runProblemPromisePending,
  runProblemPromiseFulfilled,
  runProblemPromiseRejected,
  startRunPoll,
  stopRunPoll,
  markFileAsChanged,
  saveProblemPromisePending,
  saveProblemPromiseFulfilled,
  saveProblemPromiseRejected
} from './problemsAction';
import {showError} from '../../widgets/notification/notificationAction';
import {
  apiRequest,
  delay,
  isPollSuccessful,
  isPollFailure,
  isPollCompleted
} from '../../utils/apiUtils';
import apiSettings from '../../config/apiSettings';
import loginActionTypes from '../login/loginActionTypes';
import {
  lockUserOut
} from '../login/loginAction';

const SAVE_ERROR = 'SAVE_ERROR';
const MAX_POLL_COUNT = 30;

function* getFileNames(action) {
  try {
    const problemId = action.data.problemId;
    yield put(fetchFileNamesPending());
    const {result} = yield call(apiRequest, {
      url: apiSettings.problem(problemId),
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET'
      }
    });
    const sourceFiles = result.sourceDTOs.reduce((acc, file) => {
      acc[file.sourceId] = file;
      return acc;
    }, {});
    const testFiles = result.testDTOs.reduce((acc, file) => {
      acc[file.testId] = file;
      return acc;
    }, {});
    const sourceFileId = Object.keys(sourceFiles)[0];
    yield (put(setFilesToProblem({problemId, sourceFiles, testFiles})));
    yield (put(setActiveFile({problemId, isSourceFile: true, fileId: sourceFileId})));
    yield (put(fetchFile({problemId, isSourceFile: true, fileId: sourceFileId})));
    yield put(fetchFileNamesFulfilled());
  } catch (err) {
    if (err.errors === ERRORS.UNAUTHORIZED) {
      yield put(lockUserOut());
    } else {
      yield put(showError());
    }
    yield put(fetchFileNamesRejected());
  }
}

function* getFileContent(action) {
  try {
    const {problemId, isSourceFile, fileId} = action.data;
    yield put(fetchFilePromisePending());
    const {result} = yield call(apiRequest, {
      url: apiSettings.getFileContent(problemId, isSourceFile, fileId),
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'text/plain'
        },
        method: 'GET'
      }
    });
    yield (put(setContentToFile({problemId, isSourceFile, fileId, result})));
    yield put(fetchFilePromiseFulfilled());
  } catch (err) {
    if (err.errors === ERRORS.UNAUTHORIZED) {
      yield put(lockUserOut());
    } else {
      yield put(showError());
    }
    yield put(fetchFilePromiseRejected());
  }
}

function* setResult(operationId, resultByOperationId) {
  const problemId = yield select(getProblemIdByOperationIdSelector, operationId);
  const testResult = resultByOperationId[operationId];
  if (!problemId) {
    return false;
  }
  const status = testResult.operationStatus;
  if (!isPollCompleted(status)) {
    return false;
  }
  if (isPollSuccessful(status)) {
    yield put(runProblemPromiseFulfilled(problemId));
  } else if (isPollFailure(status)) {
    yield put(runProblemPromiseRejected(problemId));
  }
  yield put(removeItemFromPoll({problemId, operationId}));
  yield put(setTestResult({
    problemId,
    testResult
  }));
  return true;
}

function* runProblem(action) {
  const problemId = action.data.problemId;
  try {
    yield put(runProblemPromisePending(problemId));
    const {result} = yield call(apiRequest, {
      url: apiSettings.runProblem(problemId),
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'text/plain'
        },
        method: 'PUT'
      }
    });

    const operationId = result.operationId;
    yield put(addItemsToPoll({problemId, operationId}));
    yield put(startRunPoll());
  } catch (err) {
    if (err.errors === ERRORS.UNAUTHORIZED) {
      yield put(lockUserOut());
    } else {
      yield put(showError());
      yield put(runProblemPromiseRejected((problemId)));
    }
  }
}

function* checkForPollingCount(pollingItemsId, timesTriedForOperationId, resultByOperationId) {
  const pollCountForOperationId = timesTriedForOperationId;
  for (let i = 0; i < pollingItemsId.length; i += 1) {
    const operationId = pollingItemsId[i];
    let pollCount = pollCountForOperationId[operationId];
    pollCount = pollCount || 0;
    pollCount += 1;

    if (pollCount > MAX_POLL_COUNT) {
      delete pollCountForOperationId[operationId];
      const problemId = yield select(getProblemIdByOperationIdSelector, operationId);
      yield put(removeItemFromPoll({problemId, operationId}));
      yield put(runProblemPromiseRejected(problemId));
      const testResult = resultByOperationId[operationId];
      testResult.status = 'FAILURE';
      testResult.errorInformation = 'Something went wrong. Please make sure your code does not contain infinite loops and try again...';
      yield put(setTestResult({
        problemId,
        testResult
      }));
    } else {
      pollCountForOperationId[operationId] = pollCount;
    }
  }
}

function* runProblemPollSagaWorker() {
  const timesTriedForOperationId = {};
  while (true) {
    yield call(delay, pollIntervals.RUN_POLL);
    try {
      const pollingItems = yield select(getItemsToPollSelector);
      const pollingItemsId = Object.keys(pollingItems);
      if (!pollingItemsId.length) {
        yield put(stopRunPoll());
      }

      const {result} = yield call(apiRequest, {
        url: apiSettings.poll(pollingItemsId),
        options: {
          credentials: 'include',
          headers: {
            'Content-Type': 'text/plain'
          },
          method: 'GET'
        }
      });
      const resultByOperationId = result.reduce((acc, operation) => {
        acc[operation.operationId] = operation;
        return acc;
      }, {});
      const operationIds = Object.keys(resultByOperationId);
      yield all(operationIds.map(id => call(setResult, id, resultByOperationId)));
      yield checkForPollingCount(pollingItemsId, timesTriedForOperationId, resultByOperationId);
    } catch (err) {
      if (err.errors === ERRORS.UNAUTHORIZED) {
        yield put(lockUserOut());
      }
      console.error(err);
    }
  }
}

function* saveFile(problemId, file) {
  const fileId = file.id;
  const fileContent = file.content;
  try {
    const {result} = yield call(apiRequest, {
      url: apiSettings.saveFile(problemId, fileId),
      options: {
        credentials: 'include',
        headers: {
          'Content-Type': 'text/plain'
        },
        method: 'PUT',
        body: fileContent
      }
    });
    yield put(markFileAsChanged({problemId, isSourceFile: true, fileId, isDirty: false}));
    return result;
  } catch (err) {
    if (err.errors === ERRORS.UNAUTHORIZED) {
      yield put(lockUserOut());
    }
    return SAVE_ERROR;
  }
}

function* saveProblem(action) {
  const problemId = action.data.problemId;
  const files = yield select(getUnsavedFilesSelector, problemId);
  yield put(saveProblemPromisePending(problemId));
  const results = yield all(files.map(file => call(saveFile, problemId, file)));
  if (results.includes(SAVE_ERROR)) {
    yield put(saveProblemPromiseRejected(problemId));
    yield put(showError());
  } else {
    let ismalicousCode = false;
    let message = '';
    if (results[0].message && results[0].message.includes('malicious code')) {
      ismalicousCode = true;
      message = results[0].message;
    }
    yield put(saveProblemPromiseFulfilled(problemId, ismalicousCode, message));
  }
}

function* fetchFileSaga() {
  yield takeEvery(problemsActionTypes.FETCH_FILE, getFileContent);
}

function* loadActiveProblemSaga() {
  yield takeEvery(problemsActionTypes.LOAD_ACTIVE_PROBLEM, getFileNames);
}

function* runProblemSaga() {
  yield takeEvery(problemsActionTypes.RUN_PROBLEM, runProblem);
}

function* saveProblemSaga() {
  yield takeLatest(problemsActionTypes.SAVE_PROBLEM, saveProblem);
}

function* runProblemPollSaga() {
  while (true) {
    yield take(problemsActionTypes.START_RUN_POLL);
    yield race({
      task: call(runProblemPollSagaWorker),
      cancel: take([problemsActionTypes.STOP_RUN_POLL,
        loginActionTypes.LOGOUT])
    });
  }
}

export {loadActiveProblemSaga, fetchFileSaga, runProblemSaga, runProblemPollSaga, saveProblemSaga};
