import deepClone from 'deep-clone';
import problemsInitialState from './problemsInitialState';
import problemsActionTypes from './problemsActionTypes';
import {setPromiseState} from '../../utils/apiUtils';

let undoManagerId = 0;

const problemsReducer = (state = problemsInitialState, action) => {
  switch (action.type) {
    case problemsActionTypes.SET_PROBLEMS:
      return {...state, items: action.data.problems};
    case problemsActionTypes.FILENAMES_FETCH.pending:
      return {...state, fileNamesFetchPromise: setPromiseState(true, false, false)};
    case problemsActionTypes.FILENAMES_FETCH.fulfilled:
      return {...state, fileNamesFetchPromise: setPromiseState(false, true, false)};
    case problemsActionTypes.FILENAMES_FETCH.rejected:
      return {...state, fileNamesFetchPromise: setPromiseState(false, false, true)};
    case problemsActionTypes.FETCH_FILE_PROMISE.pending:
      return {...state, fetchFilePromise: setPromiseState(true, false, false)};
    case problemsActionTypes.FETCH_FILE_PROMISE.fulfilled:
      return {...state, fetchFilePromise: setPromiseState(false, true, false)};
    case problemsActionTypes.FETCH_FILE_PROMISE.rejected:
      return {...state, fetchFilePromise: setPromiseState(false, false, true)};
    case problemsActionTypes.SET_FILES_TO_PROBLEM: {
      const {problemId, src, test} = action.data;
      const selectedProblem = state.items[problemId];
      const clonedState = deepClone(state);
      clonedState.items[problemId] = {...selectedProblem, src: {...src}, test: {...test}};
      return clonedState;
    }
    case problemsActionTypes.SET_CONTENT_TO_FILE: {
      const {problemId, fileType, fileId, result} = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId][fileType][fileId].content = result;
      if (!clonedState.items[problemId][fileType][fileId].undoManagerState) {
        undoManagerId += 1;
        const initialUndoManagerState = {id: undoManagerId, undoStack: [], redoStack: []};
        clonedState.items[problemId][fileType][fileId].undoManagerState = initialUndoManagerState;
      }
      return clonedState;
    }
    case 'PROBLEM_SAVE_UNDO_MANAGER' : {
      const {problemId, fileType, fileId, undoManagerState} = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId][fileType][fileId].undoManagerState = undoManagerState;
      return clonedState;
    }
    case problemsActionTypes.SET_ACTIVE_FILE: {
      const {problemId, fileType, fileId} = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId].activeFile = {
        fileId,
        fileType
      };
      return clonedState;
    }
    case problemsActionTypes.RUN_PROBLEM_PROMISE.pending: {
      const problemId = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId].runProblemPromise = setPromiseState(true, false, false);
      return clonedState;
    }
    case problemsActionTypes.RUN_PROBLEM_PROMISE.fulfilled: {
      const problemId = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId].runProblemPromise = setPromiseState(false, true, false);
      return clonedState;
    }
    case problemsActionTypes.RUN_PROBLEM_PROMISE.rejected: {
      const problemId = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId].runProblemPromise = setPromiseState(false, false, true);
      return clonedState;
    }
    case problemsActionTypes.ADD_ITEM_TO_POLL: {
      const {problemId, operationId} = action.data;
      const clonedState = deepClone(state);
      const pollingItems = clonedState.items[problemId].pollingItems || {};
      clonedState.items[problemId].pollingItems = {...pollingItems, [operationId]: problemId};
      return clonedState;
    }
    case problemsActionTypes.REMOVE_ITEM_FROM_POLL: {
      const {problemId, operationId} = action.data;
      const clonedState = deepClone(state);
      delete clonedState.items[problemId].pollingItems[operationId];
      return clonedState;
    }
    case problemsActionTypes.SET_TEST_RESULT: {
      const clonedState = deepClone(state);
      const {problemId, testResult} = action.data;
      clonedState.items[problemId].testResult = testResult;
      return clonedState;
    }
    case problemsActionTypes.MARK_FILE_AS_CHANGED: {
      const clonedState = deepClone(state);
      const {problemId, fileType, fileId, isDirty} = action.data;
      clonedState.items[problemId][fileType][fileId].isDirty = isDirty;
      return clonedState;
    }
    case problemsActionTypes.SAVE_PROBLEM_PROMISE.pending: {
      const problemId = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId].saveProblemPromise = setPromiseState(true, false, false);
      return clonedState;
    }
    case problemsActionTypes.SAVE_PROBLEM_PROMISE.fulfilled: {
      const problemId = action.data.problemId;
      const clonedState = deepClone(state);
      clonedState.items[problemId].saveProblemPromise = setPromiseState(false, true, false);
      clonedState.items[problemId].isMaliciousCode = action.data.isMaliciousCode;
      clonedState.items[problemId].message = action.data.message;
      return clonedState;
    }
    case problemsActionTypes.SAVE_PROBLEM_PROMISE.rejected: {
      const problemId = action.data;
      const clonedState = deepClone(state);
      clonedState.items[problemId].saveProblemPromise = setPromiseState(false, false, true);
      return clonedState;
    }
    default:
      return state;
  }
};

export default problemsReducer;
