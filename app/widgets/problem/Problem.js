import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {operationStatus} from '../../config/constants';
import {Button} from '../../components/Button/Button';

function conditionalClass(attempted, passed) {
  if (!attempted) return '';
  return passed ? 'is-success' : 'is-danger';
}

function renderButtonWithStatus(attempted, passed) {
  return (<Button className={conditionalClass(attempted, passed)}>
    {passed ? 'Solved' : 'Solve'}
  </Button>);
}

const Problem = (props) => {
  const {
    problemId,
    title,
    description,
    optional,
    testResult,
    passed
  } = props;

  const isAttempted = (testResult && testResult.operationStatus) || typeof (passed) === 'boolean';
  const isPassed = (testResult && testResult.operationStatus) ?
   (testResult.operationStatus === operationStatus.ACCEPTED) :
   (passed === true);

  return (
    <div className="problem column is-4">
      <article className="card is-rounded">
        <div className="card-content">
          <h1 className="subtitle is-size-4 has-text-weight-bold">
            <span className="p-l-5">{title}</span>
          </h1>
          <div className="description is-size-6">
            {description}
          </div>
          {!optional &&
          <div className="attributes">
            <span className="tag is-rounded is-primary">Mandatory</span>
          </div>
          }
          <div className="control">
            <Link className="link-anchor" to={`/challenge/${problemId}`}>
              {renderButtonWithStatus(isAttempted, isPassed)}
            </Link>
          </div>
        </div>
      </article>
    </div>
  );
};

Problem.propTypes = {
  problemId: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  optional: PropTypes.bool,
  testResult: PropTypes.object,
  passed: PropTypes.bool,
};

Problem.defaultProps = {
  problemId: 0,
  title: '',
  description: '',
  optional: false,
  testResult: {},
  passed: null
};

export default Problem;
