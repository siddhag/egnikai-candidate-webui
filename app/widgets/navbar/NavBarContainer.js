import {connect} from 'react-redux';

import NavBar from './NavBar';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
