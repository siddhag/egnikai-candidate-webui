import React from 'react';
import {NavLink} from 'react-router-dom';

import {appRoutes} from '../../config/constants';

const Navbar = () => (
  <nav className="navbar">
    <div className="navbar-brand">
      <span className="navbar-item logo is-size-2 has-text-weight-bold">
        Egnikai.
      </span>
    </div>
    <div className="tabs">
      <NavLink
        to={appRoutes.INSTRUCTIONS}
        exact
        className="tab"
        title="Instructions"
        activeClassName="active-tab"
      >
        <span className="icon icon-egnikai-icons-11" />
      </NavLink>
      <NavLink
        to={appRoutes.CHALLENGE}
        exact
        title="Challenge"
        activeClassName="active-tab"
        className="tab"
      >
        <span className="icon icon-egnikai-icons-12" />
      </NavLink>
    </div>
  </nav>
);

export default Navbar;
