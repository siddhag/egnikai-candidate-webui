import React from 'react';
import PropTypes from 'prop-types';

const Loader = (props) => {
  const {
    className,
    children
  } = props;
  return (
    <div className={`loader ${className}`}>
      {children}
    </div>
  );
};

Loader.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

Loader.defaultProps = {
  children: null,
  className: ''
};

export default Loader;
