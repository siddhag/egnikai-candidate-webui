export default {
  error: {
    default: 'Oops!! something went wrong',
    challenge: {
      RESOURCE_NOT_FOUND: 'No challenge assigned'
    }
  }
};
