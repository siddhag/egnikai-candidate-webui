const notificationActionTypes = {
  ADD: 'notification/ADD',
  REMOVE_BY_ID: 'notification/REMOVE_BY_ID',
  REMOVE_ALL: 'notification/REMOVE_ALL'
};

export default notificationActionTypes;
