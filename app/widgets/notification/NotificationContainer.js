import {connect} from 'react-redux';

import Notification from './Notification';
import {removeById, removeAll} from './notificationAction';

const mapStateToProps = state => ({
  notifications: state.notifications.items
});

const mapDispatchToProps = dispatch => ({
  removeById: (id) => {
    dispatch(removeById(id));
  },
  removeAll: () => {
    dispatch(removeAll());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
