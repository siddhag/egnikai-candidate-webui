import React from 'react';
import PropTypes from 'prop-types';

import Toast from '../../components/Toast/Toast';

const Notification = (props) => {
  const {notifications, removeById} = props;
  if (!notifications.length) {
    return null;
  }
  return (
    <section>
      {notifications.map(notification => (<Toast
        key={notification.id}
        type={notification.type}
        onClick={() => {
          removeById(notification.id);
        }}
        message={notification.message}
      />
      ))}
    </section>
  );
};

Notification.propTypes = {
  notifications: PropTypes.array,
  removeById: PropTypes.func
};

Notification.defaultProps = {
  notifications: [],
  removeById: null
};

export default Notification;
