import notificationActionTypes from './notificationActionTypes';
import toastTypes from '../../components/Toast/toastTypes';
import notificationMessages from './notificationMessages';

const add = notification => ({
  type: notificationActionTypes.ADD,
  data: {notification}
});

const removeById = id => ({
  type: notificationActionTypes.REMOVE_BY_ID,
  data: {id}
});

const removeAll = () => ({
  type: notificationActionTypes.REMOVE_ALL,
  data: {}
});

const showError = (message = notificationMessages.error.default) => add({
  message,
  type: toastTypes.ERROR,
  id: new Date().getUTCMilliseconds()
});

export {showError, removeById, removeAll};
