import notificationInitialState from './notificationInitialState';
import notificationActionTypes from './notificationActionTypes';

const notificationReducer = (state = notificationInitialState, action) => {
  switch (action.type) {
    case notificationActionTypes.ADD:
      return {...state, items: state.items.concat(action.data.notification)};
    case notificationActionTypes.REMOVE_BY_ID:
      return {
        ...state,
        items: state.items.filter(
          notification => notification.id !== action.data.id)
      };
    case notificationActionTypes.REMOVE_ALL:
      return {...state, items: []};
    default:
      return state;
  }
};

export default notificationReducer;
