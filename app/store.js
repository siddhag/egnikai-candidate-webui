import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {persistStore, persistReducer} from 'redux-persist';
import storageSession from 'redux-persist/lib/storage/session';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [
  sagaMiddleware
];

const persistConfig = {
  key: 'root',
  storage: storageSession
};


const persistedReducer = persistReducer(persistConfig, rootReducer);

const devEnv = process.env.NODE_ENV !== 'production';
const store = createStore(persistedReducer, {}, compose(
  applyMiddleware(...middlewares),
  // See github.com/zalmoxisus/redux-devtools-extension#implementation
  (devEnv && window.devToolsExtension ? window.devToolsExtension() : noOp => noOp)
));
sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);

export {store, persistor};
