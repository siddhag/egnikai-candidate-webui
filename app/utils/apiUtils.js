import { operationStatus, ERRORS } from '../config/constants';

const apiRequest = params =>
  fetch(params.url, params.options)
    .then((response) => {
      if (response.status === 401) {
        return Promise.reject({
          errors: ERRORS.UNAUTHORIZED
        });
      }
      const isJSONResponse = response.headers.get('content-type').includes('json');
      const responsePromise = isJSONResponse ? response.json() : response.text();
      return responsePromise.then((result) => {
        if (response.ok) {
          return {result};
        }
        return Promise.reject(result);
      });
    })
    .catch(errors => (Promise.reject(errors)));

const delay = ms => new Promise(res => setTimeout(res, ms));


const setPromiseState = (isPending = false, isFulfilled = false, isRejected = false) => ({
  isPending,
  isFulfilled,
  isRejected
});

const isPollSuccessful = status => status === operationStatus.ACCEPTED;
const isPollFailure = status => status === operationStatus.REJECTED;
const isPollCompleted = status => isPollSuccessful(status) || isPollFailure(status);

export {
  apiRequest, setPromiseState, delay,
  isPollSuccessful, isPollFailure, isPollCompleted
};

