import {combineReducers} from 'redux';

import loginReducer from './pages/login/loginReducer';
import challengeReducer from './pages/challenge/challengeReducer';
import notificationReducer from './widgets/notification/notificationReducer';
import loginActionTypes from './pages/login/loginActionTypes';
import problemsReducer from './pages/problems/problemsReducer';

const appReducer = combineReducers({
  login: loginReducer,
  challenge: challengeReducer,
  notifications: notificationReducer,
  problems: problemsReducer
});

const rootReducer = (state, action) => {
  let clonedState = {...state};
  if (action.type === loginActionTypes.LOGOUT) {
    clonedState = undefined;
  }
  return appReducer(clonedState, action);
};

export default rootReducer;
