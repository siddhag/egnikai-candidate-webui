import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Switch, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';

import {appRoutes} from './config/constants';
import {store, persistor} from './store';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import LoginForm from './pages/login/LoginContainer';
import Welcome from './pages/welcome/WelcomeContainer';
import Instruction from './pages/instruction/Instruction';
import Challenge from './pages/challenge/ChallengeContainer';
import MainLayout from './pages/mainLayout/MainLayoutContainer';
import ProblemsContainer from './pages/problems/ProblemsContainer';
import Thankyou from './pages/thankyou/Thankyou';

require('babel-core/register');
require('babel-polyfill');

const isLoggedIn = () => store.getState().login.isLoggedIn;

const App = () =>
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <div className="container-fluid">
        <MainLayout>
          <Switch>
            <Route
              exact
              path={appRoutes.LOGIN}
              component={LoginForm}
            />
            <PrivateRoute
              exact
              path={appRoutes.WELCOME}
              component={Welcome}
              isLoggedIn={isLoggedIn()}
            />
            <PrivateRoute
              exact
              path={appRoutes.INSTRUCTIONS}
              component={Instruction}
              isLoggedIn={isLoggedIn()}
            />
            <PrivateRoute
              exact
              path={appRoutes.CHALLENGE}
              component={Challenge}
              isLoggedIn={isLoggedIn()}
            />
            <PrivateRoute
              exact
              path={`${appRoutes.CHALLENGE}/:id`}
              component={ProblemsContainer}
              isLoggedIn={isLoggedIn()}
            />
            <Route
              exact
              path={appRoutes.THANK_YOU}
              component={Thankyou}
            />
          </Switch>
        </MainLayout>
      </div>
    </PersistGate>
  </Provider>;


ReactDOM.render((
  <HashRouter>
    <App />
  </HashRouter>
), document.querySelector('#app'));

