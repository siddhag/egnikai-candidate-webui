const operationStatus = {
  NOT_STARTED: 'NOT_STARTED',
  IN_PROGRESS: 'IN_PROGRESS',
  ACCEPTED: 'COMPLETED',
  REJECTED: 'FAILED'
};

const operationName = {
  RUN: 'RUN',
  COMPILE: 'COMPILE'
};

const ERRORS = {
  UNAUTHORIZED: 'Unauthorised'
};

const ERROR_MESSAGES = {
  LOCKED_OUT: 'You are being locked out'
};


const fileTypeNamespace = {
  SOURCE: 'src',
  TEST: 'test'
};

const pollIntervals = {
  CHALLENGE_POLL: 1000,
  RUN_POLL: 1000
};

const appRoutes = {
  LOGIN: '/',
  WELCOME: '/welcome',
  INSTRUCTIONS: '/instructions',
  CHALLENGE: '/challenge',
  THANK_YOU: '/thank-you'
};

export {
  operationStatus,
  operationName,
  fileTypeNamespace,
  pollIntervals,
  appRoutes,
  ERRORS,
  ERROR_MESSAGES
};
