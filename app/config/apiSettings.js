import CONFIG from '../config.jsenv';

const baseRoute = CONFIG.REACT_APP_API_URL || 'http://localhost:8080/api/';
const PATH = {
  SOURCE: 'sources',
  TEST: 'tests'
};
const getFileTypePath = isSourceFile => (isSourceFile ?
  PATH.SOURCE : PATH.TEST);

const apiSettings = {
  login: `${baseRoute}login`,
  logout: `${baseRoute}logout`,
  initChallenge: `${baseRoute}candidate/challenge`,
  getChallenge: `${baseRoute}challenges`,
  problem(problemId) {
    return `${baseRoute}problems/${problemId}`;
  },
  runProblem(problemId) {
    return `${baseRoute}/candidate/problems/${problemId}`;
  },
  getFileContent(problemId, isSourceFile, fileId) {
    return `${this.problem(problemId)}/${getFileTypePath(isSourceFile)}/${fileId}`;
  },
  saveFile(problemId, fileId) {
    return `${this.problem(problemId)}/${PATH.SOURCE}/${fileId}`;
  },
  poll(operationIds) {
    return `${baseRoute}candidate/operation?operationId=${operationIds.join('&operationId=')}`;
  },
  getAttemptedProblems: `${baseRoute}candidate/attemptedProblems`,
};

export default apiSettings;
