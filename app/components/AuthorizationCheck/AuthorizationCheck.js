import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {appRoutes} from '../../config/constants';

const AuthorizationCheck = (Component) => {
  const WrappedComponent = (props) => {
    const {isLocked} = props;
    if (isLocked) {
      return (
        <Redirect to={{pathname: appRoutes.LOGIN, state: {from: props.location}}} />
      );
    }
    return <Component {...props} />;
  };
  WrappedComponent.propTypes = {
    isLocked: PropTypes.bool,
    location: PropTypes.object,
  };
  WrappedComponent.defaultProps = {
    isLocked: false,
    location: {},
  };
  return WrappedComponent;
};

export default AuthorizationCheck;
