import React from 'react';
import PropTypes from 'prop-types';

import toastTypes from './toastTypes';

const Toast = (props) => {
  const {message, onClick, type, ...others} = props;
  const classNameList = {
    [toastTypes.ERROR]: 'is-danger',
    [toastTypes.SUCCESS]: 'is-success',
    [toastTypes.DEFAULT]: 'is-info',
  };
  return (
    <div
      {...others}
      className={`Toast button is-hovered ${classNameList[type]}`}
    >
      <div className="content"> {message} </div>
      <button
        className="close button"
        onClick={onClick}
      > X
      </button>
    </div>
  );
};

Toast.propTypes = {
  message: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string
};

Toast.defaultProps = {
  message: '',
  onClick: null,
  type: toastTypes.DEFAULT
};

export default Toast;
