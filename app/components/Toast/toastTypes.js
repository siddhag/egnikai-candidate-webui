const toastTypes = {
  ERROR: 'ERROR',
  SUCCESS: 'SUCCESS',
  DEFAULT: 'DEFAULT'
};

export default toastTypes;
