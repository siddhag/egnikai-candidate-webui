import React from 'react';
import PropTypes from 'prop-types';

const Modal = props => (
  <div className="loader">
    <div className="modal">
      {props.children}
    </div>
  </div>
);


Modal.propTypes = {
  children: PropTypes.node,
};

Modal.defaultProps = {
  children: null,
};

export default Modal;
