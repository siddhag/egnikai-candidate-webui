import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const {
    children,
    className,
    onClick,
    ...others
  } = props;

  return (
    <button {...others} className={`button is-primary ${className}`} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
  disabled: PropTypes.bool
};

Button.defaultProps = {
  children: null,
  onClick: null,
  className: '',
  disabled: false
};

export {
  Button
};
