import React from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';
import 'brace/mode/java';
import 'brace/mode/csharp';
import 'brace/theme/eclipse';
import 'brace/ext/language_tools';

class CodeEditor extends React.Component {
  static filterHistory(deltas) {
    return deltas.filter(d => d.group !== 'fold');
  }

  constructor(props) {
    super(props);
    this.editorRef = React.createRef();
    this.saveState = this.saveState.bind(this);
    this.serializeUndoStateFromProps2 = this.serializeUndoStateFromProps2.bind(this);
  }

  componentDidMount() {
    this.deserializeUndoStateFromProps();
    const saveCommand = {
      name: 'save',
      bindKey: {win: 'Ctrl-S', mac: 'Cmd-S'}
    };
    saveCommand.exec = () => {
      this.props.saveFile();
    };

    this.editorRef.current.editor.commands.addCommand(saveCommand);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.undoManagerState || !this.props.undoManagerState ||
       prevProps.undoManagerState.id !== this.props.undoManagerState.id) {
      this.saveState(prevProps);
      this.deserializeUndoStateFromProps();
    }
  }

  componentWillUnmount() {
    this.saveState(this.props);
  }

  saveState(prevProps) {
    if (prevProps.undoManagerState) {
      const undoManagerState = this.serializeUndoStateFromProps2();
      undoManagerState.id = prevProps.undoManagerState.id;
      this.props.saveUndoManagerState({
        problemId: prevProps.problemId,
        fileId: prevProps.activeFile.fileId,
        fileType: prevProps.activeFile.fileType,
        undoManagerState
      });
    }
  }

  deserializeUndoStateFromProps() {
    const undoManager = this.editorRef.current.editor.getSession().getUndoManager();
    undoManager.reset();

    if (!this.props.undoManagerState) {
      return;
    }


    const {undoStack = [], redoStack = []} = this.props.undoManagerState;
    undoManager.$doc = this.editorRef.current.editor.getSession();
    undoManager.$undoStack = undoStack;
    undoManager.$redoStack = redoStack;

    this.editorRef.current.editor.getSession().setUndoManager(undoManager);
  }

  serializeUndoStateFromProps() {
    const undoManager = this.editorRef.current.editor.getSession().getUndoManager();
    const undoStack = undoManager.$undoStack.map(CodeEditor.filterHistory);
    const redoStack = undoManager.$redoStack.map(CodeEditor.filterHistory);

    const id = this.props.undoManagerState.id;
    return {id, undoStack, redoStack};
  }

  serializeUndoStateFromProps2() {
    const undoManager = this.editorRef.current.editor.getSession().getUndoManager();
    const undoStack = undoManager.$undoStack.map(CodeEditor.filterHistory);
    const redoStack = undoManager.$redoStack.map(CodeEditor.filterHistory);

    return { undoStack, redoStack};
  }

  render() {
    return (
      <div className={`${this.props.readOnly ? 'read-only-mode' : ''} editor`}>
        <AceEditor
          ref={this.editorRef}
          {...this.props}
          theme="eclipse"
          name="codeEditor"
          width="100%"
          fontSize={16}
          wrapEnabled
          enableBasicAutocompletion
          showPrintMargin={false}
          showGutter
          highlightActiveLine
          editorProps={{$blockScrolling: Infinity}}
          setOptions={{
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            enableSnippets: false,
            showLineNumbers: true,
            tabSize: 2,
          }}
        />
      </div>);
  }

}

CodeEditor.propTypes = {
  undoManagerState: PropTypes.object,
  saveUndoManagerState: PropTypes.func.isRequired,
  saveFile: PropTypes.func.isRequired,
  readOnly: PropTypes.bool
};

CodeEditor.defaultProps = {
  readOnly: false,
  saveUndoManagerState: null,
  undoManagerState: null
};

export default CodeEditor;

