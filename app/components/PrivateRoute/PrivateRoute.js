import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import {appRoutes} from '../../config/constants';

const PrivateRoute = ({component: Component, isLoggedIn, ...rest}) => (
  <Route
    {...rest}
    render={props => (
      isLoggedIn
        ? <Component {...props} />
        : <Redirect to={{pathname: appRoutes.LOGIN, state: {from: props.location}}} />
    )}
  />
);

PrivateRoute.propTypes = {
  location: PropTypes.object,
  component: PropTypes.func,
  isLoggedIn: PropTypes.bool
};

PrivateRoute.defaultProps = {
  location: {},
  component: null,
  isLoggedIn: false
};

export default PrivateRoute;
