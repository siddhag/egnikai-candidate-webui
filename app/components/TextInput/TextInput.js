import React, {Component} from 'react';
import PropTypes from 'prop-types';

class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    const value = e.currentTarget.value;
    this.setState({
      value
    });
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  }

  render() {
    const {
      className,
      id,
      labelText,
      ...others
    } = this.props;
    return (
      <div className="TextInput">
        <input
          id={id}
          {...others}
          required
          className={`TextInput-input input is-size-6 ${className}`}
          value={this.state.value}
          onChange={this.onChange}
        />
        <label htmlFor={id} className="TextInput-label is-size-6">{labelText}</label>
        <i className="TextInput-bar" />
      </div>
    );
  }
}

TextInput.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  labelText: PropTypes.string,
  onChange: PropTypes.func
};

TextInput.defaultProps = {
  className: '',
  disabled: false,
  type: 'text',
  labelText: 'Label',
  onChange: null,
  id: Math.random().toString(36).substr(2, 9) // unique ID generator
};

export default TextInput;
