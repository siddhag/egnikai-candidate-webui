const fs = require('fs');

exports.getSource = (req, res) => {
  setTimeout(() => {
    fs.readFile('./data/fileContent.txt', 'utf8', (err, data) => {
      if (err) throw err;
      res.setHeader('content-type', 'text/plain');
      res.send(data);
    });
  }, 1000);
};

exports.saveSource = (req, res) => {
  setTimeout(() => {
    res.send({
      message: 'Successfully placed the save message on queue'
    });
  }, 1000);
};


exports.getTest = (req, res) => {
  setTimeout(() => {
    fs.readFile('./data/testContent.txt', 'utf8', (err, data) => {
      if (err) throw err;
      res.setHeader('content-type', 'text/plain');
      res.send(data);
    });
  }, 1000);
};


exports.getFileNames = (req, res) => {
  setTimeout(() => {
    res.send(require(`../data/fileNames`));
  }, 1000);
};
