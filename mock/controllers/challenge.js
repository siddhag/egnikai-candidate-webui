exports.init = (req, res) => {
  setTimeout(() => {
    res.send({
      operationId: 1
    });
  }, 1000);
};

exports.getChallenge = (req, res) => {
  setTimeout(() => {
    res.send(require(`../data/challenges`));
    // res.status(404).send({
    //   "errors": [
    //     {
    //       "id": "14fc7cf4-bbcc-4d82-9c29-26fd15ce87c5",
    //       "status": "404",
    //       "code": "RESOURCE_NOT_FOUND"
    //     }
    //   ]
    // });
  }, 1000);
};

exports.attemptedProblems = (req, res) => {
  setTimeout(() => {
    res.send(require(`../data/attempted-problems`));
  }, 1000);
};

exports.run = (req, res) => {
  setTimeout(() => {
    res.send({
      operationId: req.params.problemId || 1,
    });
  }, 1000);
};

exports.poll = (req, res) => {
  setTimeout(() => {
    res.send(require(`../data/poll`));
  }, 1000);
};
