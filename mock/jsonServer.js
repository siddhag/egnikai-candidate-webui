const jsonServer = require('json-server');
const bodyParser = require('body-parser');

const loginController = require('./controllers/login');
const logoutController = require('./controllers/logout');
const challengeController = require('./controllers/challenge');
const filesController = require('./controllers/files');

const server = jsonServer.create();
const PORT = 8080;

server.use(jsonServer.defaults());
server.use(bodyParser.text());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: false}));

server.post('*/login', loginController.login);
server.post('*/logout', logoutController.logout);
server.put('*/candidate/challenge', challengeController.init);
server.get('*challenges', challengeController.getChallenge);
server.get('*/problems/:problemId', filesController.getFileNames);
server.get('*/problems/:problemId/sources/:sourceId', filesController.getSource);
server.put('*/problems/:problemId/sources/:sourceId', filesController.saveSource);
server.get('*/problems/:problemId/tests/:testId', filesController.getTest);
server.put('*/problems/:problemId', challengeController.run);
server.get('*/candidate/operation?', challengeController.poll);

server.get('*challenges/attempted-problems', challengeController.attemptedProblems);

server.listen(PORT, () => {
  console.log(`JSON Mock Server is running on port ${PORT}`);
});
